<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit7f9f0994c8fb6557b031e8010c01de27
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/BITM/SEIP136261',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit7f9f0994c8fb6557b031e8010c01de27::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit7f9f0994c8fb6557b031e8010c01de27::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
